﻿namespace alarm_clock
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelTime = new System.Windows.Forms.Label();
            this.maskedTextBoxAlarm = new System.Windows.Forms.MaskedTextBox();
            this.buttonSetAlarm = new System.Windows.Forms.Button();
            this.buttonStopAlarm = new System.Windows.Forms.Button();
            this.timerTime = new System.Windows.Forms.Timer(this.components);
            this.timerAlarm = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.listBoxAlarm = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTime.Location = new System.Drawing.Point(88, 32);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(212, 55);
            this.labelTime.TabIndex = 0;
            this.labelTime.Text = "00:00:00";
            // 
            // maskedTextBoxAlarm
            // 
            this.maskedTextBoxAlarm.BeepOnError = true;
            this.maskedTextBoxAlarm.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maskedTextBoxAlarm.Location = new System.Drawing.Point(128, 120);
            this.maskedTextBoxAlarm.Mask = "00:00";
            this.maskedTextBoxAlarm.Name = "maskedTextBoxAlarm";
            this.maskedTextBoxAlarm.Size = new System.Drawing.Size(128, 62);
            this.maskedTextBoxAlarm.TabIndex = 1;
            this.maskedTextBoxAlarm.ValidatingType = typeof(System.DateTime);
            this.maskedTextBoxAlarm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.maskedTextBoxAlarm_KeyPress);
            // 
            // buttonSetAlarm
            // 
            this.buttonSetAlarm.Location = new System.Drawing.Point(16, 200);
            this.buttonSetAlarm.Name = "buttonSetAlarm";
            this.buttonSetAlarm.Size = new System.Drawing.Size(176, 48);
            this.buttonSetAlarm.TabIndex = 2;
            this.buttonSetAlarm.Text = "Установить будильник";
            this.buttonSetAlarm.UseVisualStyleBackColor = true;
            this.buttonSetAlarm.Click += new System.EventHandler(this.buttonSetAlarm_Click);
            // 
            // buttonStopAlarm
            // 
            this.buttonStopAlarm.Location = new System.Drawing.Point(208, 200);
            this.buttonStopAlarm.Name = "buttonStopAlarm";
            this.buttonStopAlarm.Size = new System.Drawing.Size(176, 48);
            this.buttonStopAlarm.TabIndex = 3;
            this.buttonStopAlarm.Text = "Выключить будильник";
            this.buttonStopAlarm.UseVisualStyleBackColor = true;
            this.buttonStopAlarm.Click += new System.EventHandler(this.buttonStopAlarm_Click);
            // 
            // timerTime
            // 
            this.timerTime.Interval = 1000;
            this.timerTime.Tick += new System.EventHandler(this.timerTime_Tick);
            // 
            // timerAlarm
            // 
            this.timerAlarm.Interval = 1000;
            this.timerAlarm.Tick += new System.EventHandler(this.timerAlarm_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(144, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Текущее время";
            // 
            // listBoxAlarm
            // 
            this.listBoxAlarm.FormattingEnabled = true;
            this.listBoxAlarm.Location = new System.Drawing.Point(16, 264);
            this.listBoxAlarm.Name = "listBoxAlarm";
            this.listBoxAlarm.Size = new System.Drawing.Size(368, 95);
            this.listBoxAlarm.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 385);
            this.Controls.Add(this.listBoxAlarm);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonStopAlarm);
            this.Controls.Add(this.buttonSetAlarm);
            this.Controls.Add(this.maskedTextBoxAlarm);
            this.Controls.Add(this.labelTime);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Будильник";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxAlarm;
        private System.Windows.Forms.Button buttonSetAlarm;
        private System.Windows.Forms.Button buttonStopAlarm;
        private System.Windows.Forms.Timer timerTime;
        private System.Windows.Forms.Timer timerAlarm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBoxAlarm;
    }
}

