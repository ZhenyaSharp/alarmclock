﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace alarm_clock
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            soundPlayer = new SoundPlayer("gman_riseshine.wav");
        }

        private SoundPlayer soundPlayer;


        private void Form1_Load(object sender, EventArgs e)
        {
            buttonStopAlarm.Enabled = false;
            timerTime.Start();
        }

        private void timerTime_Tick(object sender, EventArgs e)
        {
            labelTime.Text = DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00") + ":" +
                             DateTime.Now.Second.ToString("00");
        }

        private void buttonSetAlarm_Click(object sender, EventArgs e)
        {
            int hours, minutes;

            if (maskedTextBoxAlarm.Text == "  :")
            {
                MessageBox.Show("Ошибка. Введите время");
                return;
            }

            hours = int.Parse(maskedTextBoxAlarm.Text.Substring(0, 2));
            minutes = int.Parse(maskedTextBoxAlarm.Text.Substring(3, 2));

            if (hours > 23 || minutes > 59)
            {
                MessageBox.Show("Ошибка. Некорректное время будильника");
                maskedTextBoxAlarm.Clear();
                return;
            }

            listBoxAlarm.Items.Add(maskedTextBoxAlarm.Text+":00");
            buttonStopAlarm.Enabled = true;
            timerAlarm.Start();
        }

        private bool CheckAlarmTime()
        {
            return listBoxAlarm.Items.Contains(labelTime.Text);
        }

        private void timerAlarm_Tick(object sender, EventArgs e)
        {
            if (CheckAlarmTime())
            {
                soundPlayer.PlayLooping();
            }
        }

        private void buttonStopAlarm_Click(object sender, EventArgs e)
        {
            soundPlayer.Stop();
            timerAlarm.Stop();
            maskedTextBoxAlarm.Enabled = true;
            buttonStopAlarm.Enabled = false;
        }

        private void maskedTextBoxAlarm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Space)
            {
                e.KeyChar = '\0';
                MessageBox.Show("Введите без пробелов в формате(00:00)");
            }
        }
    }
}
